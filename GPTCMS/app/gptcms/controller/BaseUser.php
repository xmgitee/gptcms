<?php

namespace app\gptcms\controller;

use app\BaseController;
use think\facade\Db;
use think\Request;
use think\facade\Session;

class BaseUser extends BaseController
{
    protected $req;
    protected $user;
    protected $wid;
    protected $token;


    public function  __construct(Request $request){
        $this->req = $request;
        $this->host  = $request->host();
        $url  = $request->url();
        $this->token = $this->req->header('UserToken');
        if(strpos($this->token,'_')){
            $arrToken = explode('_', $this->token);
            $this->token = $arrToken[0];
        }

        $this->user = Db::table('kt_base_user')->where([['token', '=', $this->token], ['expire_time', '>',time()]])->find();
        if($this->user){
            $this->wid = $this->user['id'];
            Session::set('wid',$this->user['id']);
            Session::set('uid',$this->user['agid']);
        } 
    }

    /**
     * 获取用户后台前端路由配置
     */
    public function getUserRoutes()
    {
        $userroutes = include app_path()."userroutes.php";
        $show = $this->req->param('show',0);
        $user = $this->user;
        if($show==1) return success('OK',$userroutes);
        $auths = $this->getAllPath(); //用户有权限的
        $noAuths = $this->getNoInstallPath(); //用户不能有的权限
        $auths = array_diff($auths,$noAuths);
        $newRoutes = [];
        foreach ($userroutes as $value) {
            if($value['path'] == '/dashboard/index'){
                $newRoutes[] = $value;
                continue;    
            } 
            $children1 = [];
            if($value['children']){
                foreach ($value['children'] as $cValue1) {
                    $children2 = [];
                    if($cValue1['children']){
                        foreach ($cValue1['children'] as $cValue2) {
                            if(in_array($cValue2['path'], $auths)){
                                $children2[] = $cValue2;
                            }
                        }
                    }
                    if($children2){
                        $cValue1['path'] = reset($children2)['path'];
                        $cValue1['children'] = $children2;
                        $children1[] = $cValue1;
                    } 
                }
            }
            if($children1){
                $value['path'] = reset($children1)['path'];
                $value['children'] = $children1;
                $newRoutes[] = $value;
            }
        }
        return success('OK',$newRoutes);
    }

    /**
     * 获取所有菜单路径
     */
    public function getAllPath()
    {
        $userroutes = include app_path()."userroutes.php";
        $all = [];
        foreach ($userroutes as $value) {
            if($value['children']){
                foreach ($value['children'] as $cValue1) {
                    if($cValue1['children']){
                        foreach ($cValue1['children'] as $cValue2) {
                            $all[] = $cValue2['path'];
                        }
                    }
                }
            }
        }
        return $all;
    }

    /**
     * 获取没有安装插件的菜单路径
     */
    public function getNoInstallPath()
    {
        $noInstall = [];
        //判断key池是否已安装
        $gptcms_key = root_path().'/app/gptcms_key';
        if(!file_exists($gptcms_key)){
            $noInstall[] = '/more/keys'; //路径(path)值在userroutes里找
        }
        $gptcms_model = root_path().'/app/gptcms_model';
        if(!file_exists($gptcms_model)){
            $noInstall[] = '/system/model'; //路径(path)值在userroutes里找
        }
        $gptcms_card = root_path().'/app/gptcms_card';
        if(!file_exists($gptcms_card)){
            $noInstall[] = '/package/card'; //路径(path)值在userroutes里找
        }
        $gptcms_draw = root_path().'/app/gptcms_draw';
        if(!file_exists($gptcms_draw)){
            $noInstall[] = '/more/paint'; //路径(path)值在userroutes里找
        }
        $gptcms_api = root_path().'/app/gptcms_api';
        if(!file_exists($gptcms_api)){
            $noInstall[] = '/more/api'; //路径(path)值在userroutes里找
        }
        return $noInstall;
    }
}