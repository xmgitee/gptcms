<?php
return [
	[
		"title" => "语言翻译",
		"icon" => "/static/gptcms/hot/yuyanfanyi.png",
		"sort" => 4,
		"son" => [
			[
				"content" => "【有志者,事竟成】,用英语怎么说",
				"sort" => 0,
			],
		],
	],
	[
		"title" => "写作达人",
		"icon" => "/static/gptcms/hot/nantijiepo.png",
		"sort" => 3,
		"son" => [
			[
				"content" => "请帮写一首情诗",
				"sort" => 0,
			],
		],
	],
	[
		"title" => "宇宙奥义",
		"icon" => "/static/gptcms/hot/yuzouaomi.png",
		"sort" => 2,
		"son" => [
			[
				"content" => "外星人真实存在吗？",
				"sort" => 0,
			],
		],
	],
	[
		"title" => "历史谜题",
		"icon" => "/static/gptcms/hot/lishimiti.png",
		"sort" => 1,
		"son" => [
			[
				"content" => "三星堆文化来自何方？",
				"sort" => 0,
			],
		],
	],

];